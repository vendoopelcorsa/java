
public class combate {
	private int id;
	private String nombre;
	private int vidas;
	private int[] ataques = new int[equipos.equipos];
	private int defensa;
	private int dano;
	private boolean muerto;


	// constructor (punto 2 del menu crear equipos)
	combate() {
		
	}

	public boolean isMuerto() {
		return muerto;
	}

	public void setMuerto(boolean muerto) {
		this.muerto = muerto;
	}

	
	public int[] getAtaques() {
		return ataques;
	}

	public void setAtaques(int[] ataques) {
		this.ataques = ataques;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	// metodos
	public void Reset() {
		for (int a = 0; a < 4; a++) {
			ataques[a] = 0;
		}
	}

	public boolean Ataque(int salir, int valor, int misilLimit) {
		// cuenta misiles que ya has usado y si sumandolos a los nuevos da mas de 50
		// error
		int misil = 0;
		for (int i = 0; i < ataques.length; i++) {
			misil = misil + ataques[i];
		}
		misil = misil - ataques[salir];
		if (misil + valor > misilLimit) {
			System.out.println("Error, no te quedan tantos misiles. Te quedan " + (misilLimit - misil)
					+ " misiles. Vuelve a introducir misiles: (si no te quedan misiles pon 0) ");
			return true;
		} else {
			ataques[salir] = valor;
			return false;
		}
	}

	public void Defensa() {
		// para saber cuantos misiles van a defensa se resta 50 menos los misiles
		// lanzados por el equipo a otros equipos
		for (int a = 0; a < ataques.length; a++) {
			defensa = (defensa - ataques[a]);
		}
	}

	public void Defensa2() {
		System.out.println("\nDefensa: " + defensa + "\n");
		defensa = defensa / 2;
	}

	// en combate paso los arrays con los misiles lanzados de los otros equipos y se
	// resta con la defensa y la vida
	public void Combate(int atacs1[], int atacs2[], int atacs3[], int atacs4[]) {
		if (id == 1) {
			if (defensa > 0) {
				dano = (atacs1[0] + atacs2[0] + atacs3[0] + atacs4[0]) - defensa;
			} else {
				dano = atacs1[0] + atacs2[0] + atacs3[0] + atacs4[0];
			}
		} else if (id == 2) {
			if (defensa > 0) {
				dano = (atacs1[0] + atacs2[1] + atacs3[1] + atacs4[1]) - defensa;
			} else {
				dano = atacs1[0] + atacs2[1] + atacs3[1] + atacs4[1];
			}
		} else if (id == 3) {
			if (defensa > 0) {
				dano = (atacs1[1] + atacs2[1] + atacs3[2] + atacs4[2]) - defensa;
			} else {
				dano = atacs1[1] + atacs2[1] + atacs3[2] + atacs4[2];
			}
		} else if (id == 4) {
			if (defensa > 0) {
				dano = (atacs1[2] + atacs2[2] + atacs3[2] + atacs4[3]) - defensa;
			} else {
				dano = atacs1[2] + atacs2[2] + atacs3[2] + atacs4[3];
			}
		} else if (id == 5) {
			if (defensa > 0) {
				dano = (atacs1[3] + atacs2[3] + atacs3[3] + atacs4[3]) - defensa;
			} else {
				dano = atacs1[3] + atacs2[3] + atacs3[3] + atacs4[3];
			}
		}

		// se resta el da�o a la vida del equipo
		if (dano > 1) {
			vidas = vidas - dano;
		}
		//
		if (vidas <= 0) {
			vidas = 0;
		}
	}

}
