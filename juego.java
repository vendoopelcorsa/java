import java.util.Scanner;

public class juego {
	public String guanador[] = new String[100];
	public String ganador;
	public int opcio;
	int cont1 = 0;

	public juego() {
		this.ganador = ganador;
		this.opcio = opcio;
	}

	public String getGanador() {
		return ganador;
	}

	public void setGanador(String ganador) {
		this.ganador = ganador;
	}

	public int getOpcio() {
		return opcio;
	}

	public void setOpcio(int opcio) {
		this.opcio = opcio;
	}

	public void menu() {
		Scanner sc = new Scanner(System.in);

		System.out.println(
				"\n/////////////////////////\n/ 1. Jugar \t\t/\n/ 2. Crear equipos \t/\n/ 3. Reglas del juego \t/\n/ 4. Información \t/\n/ 5. Partida custom \t/\n/ 6. Salir \t\t/\n/////////////////////////");
		System.out.println("\nIntroduce un entero entre 1 y 6");
		opcio = controlScanner();
		while (opcio < 0 && opcio > 6) {

			System.out.println("Numero incorrecto. \n Introduce un entero entre 1 y 6");
			opcio = controlScanner();
		}

	}

	public String[] Setguanador() {

		guanador[cont1] = ganador;

		System.out.println("ha ganado: " + guanador[cont1]);
		cont1++;
		return guanador;

	}

	// Evita que salte error al poner letras cuando va un numero
	public static int controlScanner() {
		Scanner sc = new Scanner(System.in);
		String s;
		boolean mal = false;
		int n = 0;
		while (mal == false) {
			s = sc.nextLine();
			if (s.matches("[-+]?\\d*\\.?\\d+")) {
				n = Integer.parseInt(s);
				if (n >= 0) {
					mal = true;
					return n;
				}
			}
			if (mal == false) {
				System.out.println("Error. Solo se permiten numeros positivos.");
			}
		}
		return n;
	}

}
