import java.util.ArrayList;
import java.util.Scanner;

public class Principal {
	// ense�ar ataques (al final de la ronda)

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		juego juego1 = new juego();
		// variables
		String s;
		boolean custom = false, eliminar=false;
		int opcio = -0, turn = 1, end = 0, salir = 0, valor = 0, nombreV = 0, muerte1 = 0, muerte2 = 0, muerte3 = 0,
				muerte4 = 0, muerte5 = 0, misilLimit = 50, oleada = 0, muerto = 0;
		String[] noms = new String[5];
		// clases equipos
		ArrayList<equipos> equiposList = new ArrayList<equipos>();// new
		equipos equipo1 = new equipos();
		equipos equipo2 = new equipos();
		equipos equipo3 = new equipos();
		equipos equipo4 = new equipos();
		equipos equipo5 = new equipos();
		boolean error = false;
		// menu
		do {
			juego1.menu();

			opcio = juego1.getOpcio();
			// opciones del menu
			switch (opcio) {
			case 1:
				if (custom == false) {
					// new
					for (equipos equipo : equiposList) {
						equipo.setVidas(200);
					}
					misilLimit = 50;
				} else if (custom == true) {
					equipo1.setVidas(nombreV);
					equipo2.setVidas(nombreV);
					equipo3.setVidas(nombreV);
					equipo4.setVidas(nombreV);
					equipo5.setVidas(nombreV);
				}
				boolean muertes[] = new boolean[equiposList.size()];// new
				/*
				 * muerte1=0; muerte2=0; muerte3=0; muerte4=0; muerte5=0;
				 */
				end = 0;
				// new
				// comprobacion de nombres
				for (int i = 0; i < equiposList.size(); i++) {
					equipos equipo = equiposList.get(i);
					System.out.print("Equipo " + i + ": " + equipo.getNombre() + "\t | \t");
					if (equipo.getNombre() == "") {
						System.out.println("Para empezar crea los equipos");
						break;
					}
				}
				// Cada vez que un equipo muere, end+1
				while (end < equiposList.size()) {
					// turnos de los jugadores
					// new
					for (int i = 0; i < equiposList.size(); i++) { // recorro para que se haga cada turno
						boolean next = false;
						while (next == false) { // se repetira hasta que se pase de turno
							do {
								muerto = 0;
								System.out.println("Turno de: ( " + equiposList.get(i).getNombre()
										+ " ) \nEscoge a quien atacar y la cantidad, el resto sera para defensa.");
								for (int x = 0; x < equiposList.size(); x++) {
									System.out.print("\t\t[" + x + "]: " + equiposList.get(x).getNombre());
								}
								System.out.println("\t\t[" + equiposList.size() + "]: Salir");
								System.out.println("Atacar a: ");

								// que no salte error al poner letras
								salir = controlScanner();

								// tocaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaar
								// esto
								if (salir == 0 && muerte2 == 1 || salir == 1 && muerte3 == 1
										|| salir == 2 && muerte4 == 1 || salir == 3 && muerte5 == 1) {
									muerto = 1;
									System.out.println("Muerto");
								}
							} while (muerto == 1);

							// entrada de ataques
							if (salir < equiposList.size()) {
								do {
									error = false;
									System.out.println("Numero de misiles: ");
									valor = controlScanner();
									error = equiposList.get(i).Ataque(salir, valor, misilLimit);
								} while (error == true);
							}
							// defensa
							equiposList.get(i).setDefensa(misilLimit);
							equiposList.get(i).Defensa();
							if (salir == equiposList.size()) {
								equiposList.get(i).Defensa2();
								next = true;
							}
						}
					}

					oleada++;
					System.out.println("/////////////////////////\n/ Oleada: " + oleada + "\t\t/");

					// Entrada de numero de misiles de ataque y defensa

					equipo1.Combate(equipo2.getAtaques(), equipo3.getAtaques(), equipo4.getAtaques(),
							equipo5.getAtaques());
					if (equipo1.getVidas() > 0) {
						System.out.println("/ Vidas de " + equipo1.getNombre() + ": " + equipo1.getVidas() + "\t/");
					} else {
						System.out.println("/ Vidas de " + equipo1.getNombre() + ": " + equipo1.getVidas() + "\t\t/");
					}

					equipo2.Combate(equipo1.getAtaques(), equipo3.getAtaques(), equipo4.getAtaques(),
							equipo5.getAtaques());
					if (equipo2.getVidas() > 0) {
						System.out.println("/ Vidas de " + equipo2.getNombre() + ": " + equipo2.getVidas() + "\t/");
					} else {
						System.out.println("/ Vidas de " + equipo2.getNombre() + ": " + equipo2.getVidas() + "\t\t/");
					}

					equipo3.Combate(equipo1.getAtaques(), equipo2.getAtaques(), equipo4.getAtaques(),
							equipo5.getAtaques());
					if (equipo3.getVidas() > 0) {
						System.out.println("/ Vidas de " + equipo3.getNombre() + ": " + equipo3.getVidas() + "\t/");
					} else {
						System.out.println("/ Vidas de " + equipo3.getNombre() + ": " + equipo3.getVidas() + "\t\t/");
					}

					equipo4.Combate(equipo1.getAtaques(), equipo2.getAtaques(), equipo3.getAtaques(),
							equipo5.getAtaques());
					if (equipo4.getVidas() > 0) {
						System.out.println("/ Vidas de " + equipo4.getNombre() + ": " + equipo4.getVidas() + "\t/");
					} else {
						System.out.println("/ Vidas de " + equipo4.getNombre() + ": " + equipo4.getVidas() + "\t\t/");
					}

					equipo5.Combate(equipo1.getAtaques(), equipo2.getAtaques(), equipo3.getAtaques(),
							equipo4.getAtaques());
					if (equipo4.getVidas() > 0) {
						System.out.println("/ Vidas de " + equipo5.getNombre() + ": " + equipo5.getVidas()
								+ "\t/\n/////////////////////////");
					} else {
						System.out.println("/ Vidas de " + equipo5.getNombre() + ": " + equipo5.getVidas()
								+ "\t\t/n/////////////////////////");
					}
					
					for(equipos equipo: equiposList) { //reset del array de ataques
						equipo.Reset();
						equipo.muerte();						
					}
															
					System.out.println("Equipos vivos: " + equipos.equipos);
					System.out.println("Continuar...[Enter]");
					sc.nextLine();

					// final de la partida
					end = 0;
					for (int y = 0; y < muertes.length; y++) {
						if (muertes[y] == true) {
							end++;
						}

						if (end >= equiposList.size()) { // si mueren todos menos uno se ha ganado la partida

							if (equiposList.get(y).isMuerto() == false) {
								juego1.setGanador(equiposList.get(y).getNombre());
								juego1.Setguanador();
							}
						}
						if (end == equiposList.size()) {
							System.out.println("Empate");
						}
					}

				}

				// eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee aqui pon que se
				// eliminen los equipos para que se puedan volver a crear
				break;
			case 2: // introducir nombres del equipo
				int borrar=0;
				// new
				if(eliminar == true) {System.out.println("0.A�adir o 1.Borrar y crear de nuevo "); borrar=sc.nextInt();}
				
				if(borrar==1) {
					eliminar(equiposList);
				}
				System.out.println("Introduce numero de equipos que jugaran: ");
				equipos.equipos = sc.nextInt();
				sc.nextLine();
				for(int i = 0; i < equipos.equipos; i++ ) {
					System.out.println("Introduce el nombre del equipo ");
					equiposList.add(new equipos());
					equiposList.get(equiposList.size() - 1).setNombre(sc.nextLine());
				eliminar = true;
				}
				
			break;
			case 3: // reglas
				System.out.println(
						"\n/////////////////////////////////////////////////////////\n/ Cada ronda empiezan todos los equipos con 50 misiles\t/\n/ pueden usarse en ataque o en defensa o en ambas\t/\n/ pero defenderse cuesta el doble de misiles.\t\t/\n/ Gana el equipo que sobreviva.\t\t\t\t/\n/////////////////////////////////////////////////////////");
				System.out.println("Continuar...[Enter]");
				sc.nextLine();
				break;
			case 4:// informacion
				System.out.println(
						"\n/////////////////////////////////////////////////////////////////////////////////////////////////////////\n/ Versi�n= 1.0\t\t\t\t\t\t\t\t\t\t\t\t/\n/  Informaci�n de contacto= dcastro.ilerna@gmail.com, sergiestall99@gmail.com, joelhervera@gmail.com \t/\n/  Autores= David Castro, Sergi Estall, Joel Hervera\t\t\t\t\t\t\t/\n/////////////////////////////////////////////////////////////////////////////////////////////////////////");
				System.out.println("Continuar...[Enter]");
				sc.nextLine();
				break;
			case 5:
				noms = nomcustom();
				equipo1.setNombre(noms[0]);
				equipo1.setId(1);
				equipo2.setNombre(noms[1]);
				equipo2.setId(2);
				equipo3.setNombre(noms[2]);
				equipo3.setId(3);
				equipo4.setNombre(noms[3]);
				equipo4.setId(4);
				equipo5.setNombre(noms[4]);
				equipo5.setId(5);
				System.out.println("introduce el numero de vidas que quieres que los equipos tengan");
				nombreV = controlScanner();
				System.out.println("introduce el numero de misiles que quieres que los equipos tengan");
				misilLimit = controlScanner();

				custom = true;
				break;
			case 6:
				
				System.exit(0);
			}
		} while (opcio != 6);
	}

	public static String[] nomcustom() {
		String[] equipos = new String[5];
		Scanner sc = new Scanner(System.in);

		System.out.println("Introduce el nombre del equipo 1");
		equipos[0] = sc.nextLine();
		System.out.println("Introduce el nombre del equipo 2");
		equipos[1] = sc.nextLine();
		System.out.println("Introduce el nombre del equipo 3");
		equipos[2] = sc.nextLine();
		System.out.println("Introduce el nombre del equipo 4");
		equipos[3] = sc.nextLine();
		System.out.println("Introduce el nombre del equipo 5");
		equipos[4] = sc.nextLine();

		return equipos;
	}

	// Evita que salte error al poner letras cuando va un numero y prohibe numeros
	// negativos

	public static int controlScanner() {
		Scanner sc = new Scanner(System.in);
		String s;
		boolean mal = false;
		int n = 0;
		while (mal == false) {
			s = sc.nextLine();
			if (s.matches("[-+]?\\d*\\.?\\d+")) {
				n = Integer.parseInt(s);
				if (n >= 0) {
					mal = true;
					return n;
				}
			}
			if (mal == false) {
				System.out.println("Error. Solo se permiten numeros positivos.");
			}
		}
		return n;
	}
	public static ArrayList eliminar(ArrayList equiposList) {
		equiposList.clear();
		return equiposList;
	}
}